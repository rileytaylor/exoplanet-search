'use strict';

module.exports = {
  extends: 'octane',
  ignore: ['app/components/svg/*'],
};
