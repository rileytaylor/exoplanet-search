import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

module('Integration | Helper | table-sorting-icons', function (hooks) {
  setupRenderingTest(hooks);

  // TODO: Replace this with your real tests.
  test('it renders', async function (assert) {
    this.inputValue = '1234';

    await render(hbs`{{table-sorting-icons this.inputValue}}`);

    assert.equal(this.element.textContent.trim(), '1234');
  });
});
