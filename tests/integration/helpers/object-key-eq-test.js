import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

module('Integration | Helper | object-key-eq', function (hooks) {
  setupRenderingTest(hooks);

  test('it returns truthy properly', async function (assert) {
    this.obj = {
      thing: true,
    };

    await render(hbs`{{object-key-eq this.obj 'thing' true}}`);

    assert.equal(this.element.textContent, 'true');
  });

  test('it returns falsy properly', async function (assert) {
    this.obj = {
      thing: false,
    };

    await render(hbs`{{object-key-eq this.obj 'thing' true}}`);

    assert.equal(this.element.textContent, 'false');
  });
});
