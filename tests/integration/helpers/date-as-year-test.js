import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

module('Integration | Helper | dateAsYear', function (hooks) {
  setupRenderingTest(hooks);

  // TODO: Replace this with your real tests.
  test('it renders', async function (assert) {
    const input = new Date();

    await render(hbs`{{date-as-year input}}`);

    assert.equal(this.element.textContent.trim(), input.getFullYear.toString());
  });
});
