module.exports = {
  extends: 'stylelint-config-standard',
  plugins: [
    // 'stylelint-scss',
    'stylelint-order', // https://github.com/hudochenkov/stylelint-order
    'stylelint-config-rational-order/plugin', // https://github.com/constverum/stylelint-config-rational-order
    'stylelint-a11y', // https://github.com/YozhikM/stylelint-a11y
  ],
  rules: {
    // 'at-rule-no-unknown': null, // Scss plugin will handle this
    'at-rule-no-unknown': [
      true,
      {
        ignoreAtRules: ['tailwind', 'apply', 'screen'],
      },
    ],
    'block-no-empty': true,
    'declaration-empty-line-before': null, // Disabled for rational-order plugin to take over
    // 'scss/at-rule-no-unknown': [
    //   true,
    //   {
    //     ignoreAtRules: ['tailwind', 'apply', 'screen'],
    //   },
    // ],
    'order/properties-order': [],
    'plugin/rational-order': [
      true,
      {
        'border-in-box-model': false,
        'empty-line-between-groups': true,
      },
    ],
    'a11y/media-prefers-reduced-motion': true,
    'a11y/no-outline-none': true,
    'a11y/selector-pseudo-class-focus': true,
  },
};
