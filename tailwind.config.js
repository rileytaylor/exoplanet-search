/*
 * TailwindCSS Configuration File
 *
 * Docs: https://tailwindcss.com/docs/configuration
 * Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
 */

const defaultTheme = require('tailwindcss/defaultTheme');
const colors = require('tailwindcss/colors');
// const EmberApp = require('ember-cli/lib/broccoli/ember-app');
// const isProduction = EmberApp.env() === 'production';

module.exports = {
  // purge: {
  //   enabled: isProduction,
  //   content: [
  //     './app/index.html',
  //     './app/templates/**/*.hbs',
  //     './app/components/**/*.hbs',
  //   ],
  // },
  darkMode: 'class', // or 'media' or 'class'
  theme: {
    colors: {
      white: colors.white,
      black: colors.black,
      transparent: 'transparent',
      current: 'currentColor',
      primary: {
        light: colors.indigo[400],
        DEFAULT: colors.indigo[500],
        dark: colors.indigo[600],
      },
      accent: {
        light: colors.emerald[400],
        DEFAULT: colors.emerald[500],
        dark: colors.emerald[600],
      },
      neutral: {
        light: colors.trueGray[400],
        DEFAULT: colors.trueGray[500],
        dark: colors.trueGray[600],
        50: colors.trueGray[50],
        100: colors.trueGray[100],
        200: colors.trueGray[200],
        300: colors.trueGray[300],
        400: colors.trueGray[400],
        500: colors.trueGray[500],
        600: colors.trueGray[600],
        700: colors.trueGray[700],
        800: colors.trueGray[800],
        900: colors.trueGray[900],
      },
      success: {
        light: colors.green[500],
        DEFAULT: colors.green[600],
        dark: colors.green[700],
      },
      error: {
        light: colors.red[500],
        DEFAULT: colors.red[600],
        dark: colors.red[700],
      },
    },
    fontFamily: {
      title: ['Audiowide', ...defaultTheme.fontFamily.sans],
      display: ['Righteous', ...defaultTheme.fontFamily.sans],
      heading: ['Nunito', ...defaultTheme.fontFamily.sans],
      body: ['Nunito Sans', ...defaultTheme.fontFamily.sans],
    },
    fontWeight: {
      bolditalic: '700i',
      ...defaultTheme.fontWeight,
    },
    // TODO: Disable some of these if unused
    screens: {
      phoneWide: '36em', // 576px
      tablet: '48em', // 768px
      laptop: '62em', // 992px
      desktop: '75em', // 1200px
      ultrawide: '87.5em', // 1400px
    },
  },
  variants: {
    extend: {
      borderWidth: ['hover'],
    },
  },
  plugins: [require('@tailwindcss/forms')],
};
