import Controller from '@ember/controller';
// import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

const DATA_TABLE_COLUMNS = [
  {
    name: 'Name',
    key: 'name',
    component: 'data-table/title-subtitle-cell',
    options: {
      title: 'name',
      subtitle: 'star',
    },
  },
  {
    name: 'Distance',
    key: 'distance',
    label: 'light years',
  },
  {
    name: 'Discovery Year',
    key: 'discoveryYear',
    component: 'data-table/date-cell',
    options: {
      type: 'year',
    },
  },
];

export default class HomeController extends Controller {
  @service router;

  dataTableColumns = DATA_TABLE_COLUMNS;

  get planetsCount() {
    if (!this.model.planets) {
      return 0;
    }

    return this.model.planets.length;
  }

  @action
  filter(type) {
    console.log(`filter! -> `, type);
  }

  @action
  handleRowClick(planet) {
    this.router.transitionTo('planets.planet', planet);
  }
}
