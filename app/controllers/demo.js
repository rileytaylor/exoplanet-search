import Controller from '@ember/controller';
import { action } from '@ember/object';
import faker from 'faker';

export default class DemoController extends Controller {
  get lorem() {
    return faker.random.words(100);
  }

  @action
  logShit(val) {
    console.log('logging!', val);
  }
}
