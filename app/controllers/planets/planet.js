import Controller from '@ember/controller';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

export default class PlanetsPlanetController extends Controller {
  @service router;

  @action
  navigateBack() {
    this.router.transitionTo('planets');
  }
}
