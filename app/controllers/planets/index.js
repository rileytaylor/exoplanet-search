import Controller from '@ember/controller';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

const DATA_TABLE_COLUMNS = [
  {
    name: 'Name',
    key: 'name',
    component: 'data-table/title-subtitle-cell',
    options: {
      title: 'name',
      subtitle: 'star',
    },
  },
  {
    name: 'Distance',
    key: 'distance',
    label: 'light years',
  },
  {
    name: 'Earth masses',
    key: 'massEarth',
  },
  {
    name: 'Earth Radii',
    key: 'radiusEarth',
  },
  {
    name: 'Discovery Year',
    key: 'discoveryYear',
    component: 'data-table/date-cell',
    options: {
      type: 'year',
    },
  },
];

export default class PlanetsIndexController extends Controller {
  @service router;

  dataTableColumns = DATA_TABLE_COLUMNS;

  @tracked search = '';
  @tracked showFilters = false;
  @tracked visibleColumns = {};

  constructor() {
    super(...arguments);

    // eslint-disable-next-line no-unused-vars
    for (const col of this.dataTableColumns) {
      this.visibleColumns[col.key] = true;
    }
  }

  @action
  updateSearch(text) {
    this.search = text;
  }

  @action
  toggleFilters() {
    this.showFilters = !this.showFilters;
  }

  @action
  filter(type) {
    console.log(`filter! -> `, type);
  }

  @action
  toggleColumn(key) {
    if (this.visibleColumns[key]) {
      this.visibleColumns[key] = !this.visibleColumns[key];
    }
  }

  @action
  handleRowClick(planet) {
    this.router.transitionTo('planets.planet', planet);
  }
}
