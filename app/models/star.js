import Model, { attr } from '@ember-data/model';

export default class StarModel extends Model {
  @attr('string') name;
  @attr('string') distance;
  @attr('string') spectralType;
  @attr('string') stellarMass;
  @attr('string') stellarEffectiveTemperature;

  // // Relationships
  // @hasMany('planet') planets;

  get numberOfPlanets() {
    return this.planets.length;
  }
}
