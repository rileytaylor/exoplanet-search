import Model, { attr } from '@ember-data/model';

// https://www.spaceflightnewsapi.net/documentation#/Article
export default class ArticleModel extends Model {
  @attr('string') title;
  @attr('boolean') featured;
  @attr('string') url;
  @attr('string') imageUrl;
  @attr('string') summary;
  @attr('string') newsSite;
  @attr('date') publishedAt;
  @attr('date') updatedAt;
}
