import Model, { attr } from '@ember-data/model';

export default class PlanetModel extends Model {
  @attr('string') name;
  @attr('string') star;

  // Stats
  @attr('string') distance;
  @attr('string') orbitalPeriod;
  @attr('string') orbitSemiMajorAxis;
  @attr('string') radiusEarth;
  @attr('string') radiusJupiter;
  @attr('string') massEarth;
  @attr('string') massJupiter;

  // Discovery
  @attr('date') discoveryYear;
  @attr('string') discoveryMethod;
  @attr('string') discoverFacility;

  // // Relationships
  // @belongsTo('star') star;
}
