import EmberRouter from '@ember/routing/router';
import config from 'exoplanet/config/environment';

export default class Router extends EmberRouter {
  location = config.locationType;
  rootURL = config.rootURL;
}

Router.map(function () {
  this.route('home', { path: '/' });
  this.route('planets', function () {
    this.route('index', { path: '/' });
    this.route('planet', { path: '/planet/:planet_id' });
  });
  this.route('news');
  this.route('demo');
});
