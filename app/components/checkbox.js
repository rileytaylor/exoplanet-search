import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { assert } from '@ember/debug';

export default class CheckboxComponent extends Component {
  /**
   * Args
   * ----
   * @property {String} name
   * @public
   *
   * @property {Boolean} disabled
   * @public
   *
   * @property {Boolean} readonly
   * @public
   *
   * @property {Boolean} required
   * @public
   *
   * @property {String} success
   * @public
   *
   * @property {String} error
   * @public
   *
   * @property {String} description
   * @public
   */

  /**
   * Set the default state of the checkbox. Defaults to false.
   * @property {Boolean} default
   * @public
   */
  default = this.args.default || false;

  /**
   * Set the value when the checkbox is checked
   * @property {String|Number|Boolean} checkedValue
   * @public
   */
  checkedValue = this.args.checkedValue || true;

  /**
   * Set the value when the checkbox is unchecked
   * @property {String|Number|Boolean} uncheckedValue
   * @public
   */
  uncheckedValue = this.args.uncheckedValue || false;

  /**
   * Set the checked state of the checkbox. Defaults to the `default` if not provided.
   * @property {Boolean} checked
   * @public
   */
  @tracked checked = this.args.checked || this.default;

  /**
   * The value to set when the checkbox is checked. Can be a string, number, or boolean.
   * @property {String|Number|Boolean} value
   * @public
   */
  @tracked value = this.args.value || this.getValue();

  get hasSuccess() {
    return this.args.success;
  }

  get hasError() {
    return this.args.error;
  }

  get hasDescription() {
    return this.args.description;
  }

  get inputId() {
    assert('You must provide a name', this.args.name);

    return `${this.args.name.replace(/\s/g, '')}-checkbox`;
  }

  get themeClasses() {
    let result = 'primary';

    if (this.hasSuccess) {
      result = 'success';
    }

    if (this.hasError) {
      result = 'error';
    }

    return `input-${result} checkbox-${result}`;
  }

  getValue() {
    return this.checked ? this.checkedValue : this.uncheckedValue;
  }

  /**
   * Events
   * ------
   *
   * @event onChange
   * @param {*} event
   * @public
   */

  /**
   * Pass the input value up
   *
   * @function handleInput
   * @param {Event} event
   * @private
   */
  @action
  handleChange(event) {
    this.checked = event.target.checked;

    this.value = this.getValue();

    console.log('onCheck -> ', this.checked, this.value);

    if (!this.args.onChange) {
      return;
    }

    assert(
      'onChange of <Checkbox> must be a function',
      typeof this.args.onChange === 'function'
    );

    this.args.onChange(this.value);
  }
}
