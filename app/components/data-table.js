import Component from '@glimmer/component';
// import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class DataTableComponent extends Component {
  /**
   * Args
   * @property {array<object>} data table data
   * @property {array<object>} columns table columns
   * @property {Function} onRowClick handle row click
   * @property {String} search filter the entire table fuzzy
   * @property {Boolean} sortable disable sorting
   */

  /**
   * Column
   * name - Display name
   * key - object key
   * label - string - unit label (i.e. days, Ibs, etc.)
   * component - string - render using a specific component
   * classes - string - pass in additional classes for rendering
   */

  /**
   * Cell Components
   * - default
   * - title-subtitle
   *
   * can take in props
   * - row -> the row data
   * - key -> the column key
   * - index -> the column index
   * - label -> the label
   * - options -> the column config options
   */

  tableTheme = {
    table: 'table',
    thead: 'table_header',
    theadRow: 'table_headerRow',
    theadCell: 'table_headerCell',
    tbody: 'table_body',
    tbodyRow: 'table_bodyRow',
    tbodyCell: 'table_bodyCell',
  };

  get canSort() {
    if (this.args.sortable) {
      return this.args.sortable;
    }

    return true;
  }

  get sortSequence() {
    // if (this.args.sortSequence) {
    //   return this.args.sortSequence;
    // }

    return ['asc', 'desc', 'unsorted'];
  }

  constructor(owner, args) {
    super(owner, args);

    console.log(this.args.columns);
    console.log(this.args.data);
  }

  /**
   * Handle clicking of rows in the table
   * @param {object} data the row data
   * @param {Number} index the row index
   * @param {Event} event the event information
   */
  @action
  handleRowClick(data, index, event) {
    console.log('Row Click ->', data, index, event);
    this.args.onRowClick?.(data, index, event);
  }
}
