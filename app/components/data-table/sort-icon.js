import Component from '@glimmer/component';

export default class DataTableSortIconComponent extends Component {
  /**
   * Args
   * @property {Object} col a yeti table column definition
   */

  /**
   * Determine the proper icon to render
   * @return {String} a font awesome icon
   */
  get icon() {
    if (this.args.col.isAscSorted) {
      return 'sort-up';
    }

    if (this.args.col.isDescSorted) {
      return 'sort-down';
    }

    return 'sort';
  }
}
