import Component from '@glimmer/component';
import { assert } from '@ember/debug';

export default class ToolbarComponent extends Component {
  /**
   * Args
   *
   * @property {Boolean} isLink
   * @property {String} title
   * @property {String} route
   */

  /**
   * Whether to render the title as a link
   * @return {Boolean} Default: true
   */
  get renderAsLink() {
    if (this.args.isLink !== undefined) {
      assert(
        'isLink should be a boolean value',
        typeof this.args.isLink === 'boolean'
      );

      return this.args.isLink;
    }

    return true;
  }
}
