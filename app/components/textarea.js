import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { assert } from '@ember/debug';

export default class TextareaComponent extends Component {
  /**
   * Args
   * ----
   * @property {String} name
   * @public
   *
   * @property {Boolean} disabled
   * @public
   *
   * @property {Boolean} readonly
   * @public
   *
   * @property {Boolean} required
   * @public
   *
   * @property {String} type
   * @public
   *
   * @property {String} placeholder
   * @public
   *
   * @property {String} success
   * @public
   *
   * @property {String} error
   * @public
   *
   * @property {String} description
   * @public
   *
   * @property {Number} minLength
   * @public
   *
   * @property {Number} maxLength
   * @public
   *
   * @property {Number} rows
   * @public
   *
   * @property {String} wrap
   * @public
   *
   * @property {String} spellcheck
   * @public
   *
   * @property {Boolean} resizeable
   * @public
   */

  /**
   * @property {String} value
   * @public
   */
  @tracked value = this.args.value || '';

  /**
   * Events
   * ------
   *
   * @event onInput
   * @param {*} event
   * @public
   */

  get wrap() {
    return this.args.wrap || 'soft';
  }

  get spellcheck() {
    return this.args.spellcheck || 'default';
  }

  get isResizeable() {
    return this.args.resizeable || true;
  }

  get hasSuccess() {
    return this.args.success;
  }

  get hasError() {
    return this.args.error;
  }

  get hasDescription() {
    return this.args.description;
  }

  get inputId() {
    assert('You must provide a name', this.args.name);

    return `${this.args.name.replace(/\s/g, '')}-textarea`;
  }

  get themeClasses() {
    let result = 'primary';

    if (this.hasSuccess) {
      result = 'success';
    }

    if (this.hasError) {
      result = 'error';
    }

    return `input-${result}`;
  }

  /**
   * Pass the input value up
   *
   * @function handleInput
   * @param {Event} event
   * @private
   */
  @action
  handleInput(event) {
    this.value = event.target.value;

    if (!this.args.onInput) {
      return;
    }

    assert(
      'onInput of <Input> must be a function',
      typeof this.args.onInput === 'function'
    );

    this.args.onInput(this.value);
  }
}
