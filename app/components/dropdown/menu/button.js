import Component from '@glimmer/component';
import { action } from '@ember/object';
import { assert } from '@ember/debug';

export default class DropdownBaseMenuButtonComponent extends Component {
  /**
   * Args
   * @property {Function} onClick
   * @property {Boolean} shouldCloseMenu
   */

  /**
   * Whether to close the menu on click
   * @return {Boolean} Default: true
   */
  get shouldCloseMenu() {
    if (this.args.shouldCloseMenu !== undefined) {
      assert(
        'shouldCloseMenu should be a boolean value',
        typeof this.args.shouldCloseMenu === 'boolean'
      );

      return this.args.shouldCloseMenu;
    }

    return true;
  }

  @action
  handleClick() {
    this.args.onClick();

    if (this.shouldCloseMenu) {
      this.args.closeMenu();
    }
  }
}
