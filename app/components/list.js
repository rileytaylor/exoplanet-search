import Component from '@glimmer/component';

export default class ListComponent extends Component {
  /**
   * Args
   * @property {String} empty what to render when the list is empty
   * @property {String} itemComponent component to use for list items
   */

  /**
   * Template to render for each list item, defaulting to `list/item`
   * @return {String} a component path
   */
  get itemTemplate() {
    return this.args.itemComponent ? this.args.itemComponent : 'list/item';
  }
}
