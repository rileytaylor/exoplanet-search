import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { assert } from '@ember/debug';

export default class InputComponent extends Component {
  TYPES = [
    'text',
    'password',
    'datetime-local',
    'date',
    'month',
    'time',
    'week',
    'number',
    'email',
    'url',
    'search',
    'tel',
    'color',
  ];

  /**
   * Args
   * ----
   * @property {String} name
   * @public
   *
   * @property {Boolean} disabled
   * @public
   *
   * @property {Boolean} readonly
   * @public
   *
   * @property {Boolean} required
   * @public
   *
   * @property {String} type
   * @public
   *
   * @property {String} placeholder
   * @public
   *
   * @property {String} success
   * @public
   *
   * @property {String} error
   * @public
   *
   * @property {String} description
   * @public
   *
   * @property {Boolean} showLabel
   * @public
   */

  /**
   * @property {String} value
   * @public
   */
  @tracked value = this.args.value || '';

  /**
   * Events
   * ------
   *
   * @event onInput
   * @param {*} event
   * @public
   */

  get hasSuccess() {
    return this.args.success;
  }

  get hasError() {
    return this.args.error;
  }

  get hasDescription() {
    return this.args.description;
  }

  get inputId() {
    assert('You must provide a name', this.args.name);

    return `${this.args.name.replace(/\s/g, '')}-${this.args.type}-input`;
  }

  get themeClasses() {
    let result = 'primary';

    if (this.hasSuccess) {
      result = 'success';
    }

    if (this.hasError) {
      result = 'error';
    }

    return `input-${result}`;
  }

  get type() {
    const result = this.args.type || 'text';

    assert('type must be valid', this.TYPES.includes(result));

    return result;
  }

  get showLabel() {
    if (this.args.showLabel !== undefined) {
      assert(
        'showLabel should be a boolean value',
        typeof this.args.showLabel === 'boolean'
      );

      return this.args.showLabel;
    }

    return true;
  }

  /**
   * Pass the input value up
   *
   * @function handleInput
   * @param {Event} event
   * @private
   */
  @action
  handleInput(event) {
    this.value = event.target.value;

    if (!this.args.onInput) {
      return;
    }

    assert(
      'onInput of <Input> must be a function',
      typeof this.args.onInput === 'function'
    );

    this.args.onInput(this.value);
  }
}
