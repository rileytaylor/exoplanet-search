import Component from '@glimmer/component';
import { action } from '@ember/object';
import { assert } from '@ember/debug';

export default class ButtonComponent extends Component {
  THEMES = ['primary', 'accent', 'neutral', 'success', 'error'];

  /**
   * Args
   * ----
   * @property {Boolean} disabled
   * @public
   *
   * @property {String} theme
   * @public
   */

  /**
   * Button theme
   * @property {String} theme
   * @public
   */
  get theme() {
    if (this.args.theme && this.args.theme !== '') {
      assert('theme must be valid', this.THEMES.includes(this.args.theme));
    }

    return this.args.theme || 'neutral';
  }

  /**
   * Generate classes for when the button is disabled
   * @property {String} disabledClasses
   * @private
   */
  get disabledClasses() {
    return this.args.disabled ? 'opacity-50 cursor-not-allowed' : '';
  }

  /**
   * Events
   * ------
   * @event onClick
   * @param {*} event
   * @public
   */

  /**
   * Pass the click event up to the parent component with an event,
   * ensuring this doesn't occur when the button is disabled or a
   * function isn't provided.
   *
   * @function handleClick
   * @param {MouseEvent} event
   * @private
   */
  @action
  handleClick(event) {
    if (!this.args.onClick) {
      return;
    }

    // Don't do it it disabled
    if (this.disabled) {
      return;
    }

    assert(
      'onClick of <Button> must be a function',
      typeof this.args.onClick === 'function'
    );

    this.args.onClick(event);
  }
}
