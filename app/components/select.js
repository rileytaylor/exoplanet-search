import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { assert } from '@ember/debug';

export default class SelectComponent extends Component {
  /**
   * Args
   * ----
   * @property {String} name
   * @public
   *
   * @property {Boolean} disabled
   * @public
   *
   * @property {Boolean} readonly
   * @public
   *
   * @property {Boolean} multiple
   * @public
   *
   * @property {String} placeholder
   * @public
   *
   * @property {String} success
   * @public
   *
   * @property {String} error
   * @public
   *
   * @property {String} description
   * @public
   */

  /**
   * @property {String} value
   * @public
   */
  @tracked selected = this.args.selected || '';

  get hasSuccess() {
    return this.args.success;
  }

  get hasError() {
    return this.args.error;
  }

  get hasDescription() {
    return this.args.description;
  }

  get inputId() {
    assert('You must provide a name', this.args.name);

    return `${this.args.name.replace(/\s/g, '')}-select`;
  }

  get themeClasses() {
    let result = 'primary';

    if (this.hasSuccess) {
      result = 'success';
    }

    if (this.hasError) {
      result = 'error';
    }

    return `select-${result}`;
  }

  /**
   * Events
   * ------
   *
   * @event onChange
   * @param {*} event
   * @public
   */

  /**
   * Pass the selected option value up
   *
   * @function handleChange
   * @param {*} event
   * @private
   */
  @action
  handleChange(event) {
    this.selected = event.target.value;

    if (!this.args.onChange) {
      return;
    }

    assert(
      'onChange of <Select> must be a function',
      typeof this.args.onChange === 'function'
    );

    this.args.onChange(this.selected);
  }
}
