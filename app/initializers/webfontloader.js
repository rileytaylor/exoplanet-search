/* eslint-disable no-undef */
export function initialize(/* application */) {
  WebFont.load({
    google: {
      families: [
        'Audiowide',
        'Righteous',
        'Nunito:400,400i,700,700i',
        'Nunito Sans:400,400i,700,700i',
      ],
    },
  });
}

export default {
  initialize,
};
