import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

export default class ApplicationRoute extends Route {
  @service store;

  async model() {
    const planets = await fetch('/planets');
    const planetsData = await planets.json();

    this.store.push(planetsData);

    const articles = await fetch('/articles');
    const articlesData = await articles.json();

    this.store.push(articlesData);
  }
}
