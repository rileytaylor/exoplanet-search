import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import RSVP from 'rsvp';

export default class HomeRoute extends Route {
  @service store;

  model() {
    return RSVP.hash({
      planets: this.store.findAll('planet'),
      articles: this.store.findAll('article'),
    });
  }
}
