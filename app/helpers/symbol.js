import { helper } from '@ember/component/helper';
import { htmlSafe } from '@ember/template';

const symbols = {
  dropdown: `
    <svg class="-mr-1 ml-2 h-5 w-5" fill="currentColor" viewBox="0 0 20 20">
      <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd"/>
    </svg>`,
};

function symbol([name]) {
  return htmlSafe(symbols?.[name]);
}

export default helper(symbol);
