import { helper } from '@ember/component/helper';

/**
 * Check if a key in an object equals a value
 * @param {Array} params
 * @param {Object} params.obj the object to inspect
 * @param {String} params.key the key of the object
 * @param {Any} params.comparison the value to equate with the key
 * @returns {Boolean}
 */
function objectKeyEq([obj, key, comparison]) {
  return obj[key] === comparison;
}

export default helper(objectKeyEq);
