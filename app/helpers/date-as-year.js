import { helper } from '@ember/component/helper';
import { isDate, format } from 'date-fns';

function dateAsYear([date]) {
  if (isDate(date)) {
    return format(date, 'y');
  }

  return date;
}

export default helper(dateAsYear);
