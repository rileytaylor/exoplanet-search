import { helper } from '@ember/component/helper';
import { format, formatRelative, isBefore, subDays, isDate } from 'date-fns';

function relativeDate([date]) {
  if (isDate(date)) {
    // If the date is more than a week old, format it differently
    if (isBefore(date, subDays(new Date(), 7))) {
      return format(date, 'MMMM do, y');
    }
    // Otherwise, return the relative date
    return formatRelative(date, new Date());
  }

  return date;
}

export default helper(relativeDate);
