# exo

[![Netlify Status](https://api.netlify.com/api/v1/badges/7cee74bb-4103-4c8a-92bf-9e1c704f6b7e/deploy-status)](https://app.netlify.com/sites/vigilant-lovelace-952ce4/deploys)
[![Storybook](https://cdn.jsdelivr.net/gh/storybookjs/brand@master/badge/badge-storybook.svg)](link to site)


Search exoplanets.

## Prerequisites

You will need the following things properly installed on your computer.

* [Git](https://git-scm.com/)
* [Node.js](https://nodejs.org/) (with npm)
* [Ember CLI](https://ember-cli.com/)

## Installation

* `git clone https://gitlab.com/rileytaylor/exoplanet-search`
* `cd exoplanet-search`
* `npm install`

## Running / Development

* `npm run start`
* Visit your app at [http://localhost:4200](http://localhost:4200).
* Visit your tests at [http://localhost:4200/tests](http://localhost:4200/tests).

### Code Generators

Make use of the many generators for code, try `ember help generate` for more details.

### Important Commands

* `npm run start` -> Start the local development server
* `npm run lint` -> Run linting for hbs templates and js
* `npm run lint:fix` -> Run linting and fix errors
* `npm run test` -> Run ember testing
* `npm run build` -> Build the app for production

## Important Information

### Styles

TODO: PostCSS/Tailwind

### Linting

TODO: eslint, stylelint, ember-template-lint

## Deploying

TODO: Specify what it takes to deploy your app

### Netlify

### Heroku

This app is configured to use the nodejs and static buildpacks. To deploy, install the heroku cli and link this repo to a project.

```sh
heroku config:set NPM_CONFIG_PRODUCTION=false
heroku buildpacks:add https://github.com/heroku/heroku-buildpack-nodejs
heroku buildpacks:add https://github.com/hone/heroku-buildpack-static
```

Then, push the current git HEAD:

```sh
git push heroku HEAD:master
```

## Further Reading / Useful Links

* [ember.js](https://emberjs.com/)
* [ember-cli](https://ember-cli.com/)
* [ember-decorators](https://ember-decorators.github.io/ember-decorators/)
* [ember-truth-helpers](https://github.com/jmurphyau/ember-truth-helpers)
* [mirage](https://ember-cli-mirage.com)
* [faker](https://marak.github.io/faker.js/)
* Development Browser Extensions
  * [ember inspector for chrome](https://chrome.google.com/webstore/detail/ember-inspector/bmdblncegkenkacieihfhpjfppoconhi)
  * [ember inspector for firefox](https://addons.mozilla.org/en-US/firefox/addon/ember-inspector/)
