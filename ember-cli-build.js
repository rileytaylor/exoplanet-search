'use strict';

const EmberApp = require('ember-cli/lib/broccoli/ember-app');

module.exports = function (defaults) {
  let app = new EmberApp(defaults, {
    // Add options here
    minifyCSS: {
      enabled: false,
    },
    postcssOptions: {
      compile: {
        enabled: true,
        onlyIncluded: true,
        cacheInclude: [/.*\.(css)$/, /.tailwind\.js$/],
        plugins: [
          require('postcss-import')({ path: ['node_modules'] }),
          require('tailwindcss'),
          require('postcss-preset-env')({ autoprefixer: false }),
          require('autoprefixer'),
          // TODO: enable only in prod
          // require('cssnano')({
          //   preset: 'default',
          //   discardComments: { removeAll: true },
          // }),
          require('postcss-reporter'),
        ],
      },
    },
    stylelint: {
      linterConfig: {
        syntax: 'css',
      },
    },
    'ember-cli-storybook': {
      enableAddonDocsIntegration: true,
    },
  });

  // Use `app.import` to add additional libraries to the generated
  // output files.
  //
  // If you need to use different assets in different
  // environments, specify an object as the first parameter. That
  // object's keys should be the environment name and the values
  // should be the asset to use in that environment.
  //
  // If the library that you are including contains AMD or ES6
  // modules that you would like to import into your application
  // please specify an object with the list of modules as keys
  // along with the exports of each module as its value.

  app.import('node_modules/webfontloader/webfontloader.js');

  return app.toTree();
};
