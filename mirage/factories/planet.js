import { Factory } from 'ember-cli-mirage';
import faker from 'faker';

export default Factory.extend({
  name() {
    // return `HD 60058${i}`;
    return `HD ${faker.random.number(999999)}`;
  },
  star() {
    return 'Star Name';
  },
  distance() {
    return faker.random.number(9999);
  },
  orbitalPeriod: '185.84±0.23',
  orbitSemiMajorAxis: '0.84',
  radiusEarth: '18.647',
  radiusJupiter: '1.664',
  massEarth: '2543',
  massJupiter: '8',
  discoveryYear() {
    return faker.date.past();
  },
});
