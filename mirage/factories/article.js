import { Factory } from 'ember-cli-mirage';
import faker from 'faker';

export default Factory.extend({
  title() {
    return faker.lorem.words();
  },
  featured() {
    return faker.random.boolean();
  },
  url() {
    return faker.internet.url();
  },
  imageUrl() {
    return faker.image.cats(400, 400, true);
  },
  summary() {
    return faker.lorem.paragraph(1);
  },
  newsSite() {
    return faker.company.companyName();
  },
  publishedAt() {
    return faker.date.past();
  },
  updatedAt() {
    return faker.date.past();
  },
});
