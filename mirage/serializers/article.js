import { JSONAPISerializer } from 'ember-cli-mirage';
import { dasherize } from '@ember/string';

export default JSONAPISerializer.extend({
  typeKeyForModel(model) {
    return dasherize(model.modelName);
  },
});
