import { hbs } from 'ember-cli-htmlbars';
import { action } from '@ember/object';

export default {
  title: 'Input',
  component: 'InputComponent',
};

export const Default = () => ({
  template: hbs`
    <Input
      @name="Default"
      @placeholder="Default input"
      @value={{this.value}}
      @onInput={{this.handleInput}}
    />

    {{this.value}}
  `,
  context: {
    value: '',
    handleInput: action(function (value) {
      this.set('value', value);
    }),
  },
});

export const Types = () => hbs`
  {{#each (array 'text' 'password' 'datetime-local' 'date' 'month' 'time' 'week' 'number' 'email' 'url' 'search' 'tel' 'color') as |type| }}
    <Input
      class="my-2"
      @name={{type}}
      @type={{type}}
    />
  {{/each}}
`;

export const ErrorState = () => ({
  template: hbs`
    <Input
      @name="Username"
      @placeholder="PinkFluffyUnicorn"
      @description="max 8 characters"
      @error={{this.error}}
      @value={{this.value}}
      @onInput={{this.handleInput}}
    />

    {{this.value}}
  `,
  context: {
    value: '',
    error: '',
    handleInput: action(function (value) {
      if (value.length > 8) {
        this.set('error', "Username can't be longer than 8 characters");
      } else {
        this.set('error', '');
      }

      this.set('value', value);
    }),
  },
});

export const SuccessState = () => ({
  template: hbs`
    <Input
      @name="Username"
      @placeholder="PinkFluffyUnicorn"
      @description="Usernames must be unique"
      @success={{this.success}}
      @value={{this.value}}
      @onInput={{this.handleInput}}
    />

    {{this.value}}
  `,
  context: {
    value: '',
    success: '',
    handleInput: action(function (value) {
      if (value !== 'PinkFluffyUnicorn') {
        this.set('success', 'Username available!');
      } else {
        this.set('success', '');
      }

      this.set('value', value);
    }),
  },
});
