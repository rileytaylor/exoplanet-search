import { hbs } from 'ember-cli-htmlbars';
import { action } from '@storybook/addon-actions';

export default {
  title: 'Button',
  component: 'ButtonComponent',
};

const Template = (args) => ({
  template: hbs`
    <Button @onClick={{ fn this.onClickButton }}>
      Button
    </Button>
  `,
  context: args,
});

export const Default = Template.bind({});
Default.args = {
  theme: 'primary',
  disabled: false,
  onClickButton: action('onClick'),
};
// Default.argTypes = {
//   theme: {
//     control: 'select',
//     options: ['primary', 'accent', 'neutral', 'success', 'error'],
//   },
//   disabled: {
//     control: 'boolean',
//   },
// };

export const Themes = () => hbs`
  {{#each (array 'primary' 'accent' 'neutral' 'success' 'error') as |theme| }}
    <ButtonBase
      class="my-2"
      @theme={{theme}}
    >
      {{theme}}
    </Button>
  {{/each}}
`;

export const Disabled = Template.bind({});
Disabled.args = {
  ...Default.args,
  disabled: true,
};
