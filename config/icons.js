module.exports = function () {
  return {
    'free-solid-svg-icons': [
      'columns',
      'check',
      'chevron-left',
      'chevron-right',
      'filter',
      'sort',
      'sort-up',
      'sort-down',
      'search',
    ],
  };
};
