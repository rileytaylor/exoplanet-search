'use strict';

const browsers = ['last 2 major versions', '> 1%', 'not dead', 'not ie 11'];

// const isCI = Boolean(process.env.CI);
// const isProduction = process.env.EMBER_ENV === 'production';

// if (isCI || isProduction) {
//   browsers.push('ie 11');
// }

module.exports = {
  browsers,
};
